const defaultResHdrDecorator = (headers) => headers;
const getUserResHeadersDecorator = (options) =>
    options.userResHeadersDecorator || defaultResHdrDecorator;
const getStrippedHeaders = (options) => options.strippedHeaders || [];
const isHeaderSendOrTimeout = (ctx) => ctx.headerSent || ctx.status === 504;

const processDecoratedHeaders = (ctx, headers, strippedHeaders) => {
    Object.keys(headers)
        .filter((item) => strippedHeaders.indexOf(item) < 0)
        .filter((item) => item !== 'transfer-encoding')
        .forEach((item) => {
            ctx.set(item, headers[item]);
        });
};

const copyHeaders = async (container) => {
    const { ctx } = container.user;
    const { options, proxy } = container;

    const rsp = proxy.res;
    const strippedHeaders = getStrippedHeaders(options);
    const userResHeadersDecorator = getUserResHeadersDecorator(options);

    if (isHeaderSendOrTimeout(ctx)) {
        return container;
    }

    ctx.status = rsp.statusCode;
    const decoratedHeaders = await userResHeadersDecorator(rsp.headers);
    processDecoratedHeaders(ctx, decoratedHeaders, strippedHeaders);
    return container;
};

export default (container) =>
    new Promise((resolve) => {
        const result = copyHeaders(container);
        resolve(result);
    });
