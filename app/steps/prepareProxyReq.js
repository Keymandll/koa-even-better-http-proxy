import { asBuffer, asBufferOrString } from '../../lib/as';

const getContentLength = (body) => {
    if (Buffer.isBuffer(body)) {
        return body.length;
    }
    if (typeof body === 'string') {
        return Buffer.byteLength(body);
    }
    return 0;
};

const getBodyContent = (container) => {
    const { bodyContent } = container.proxy;
    if (!bodyContent) return bodyContent;
    return container.options.reqAsBuffer
        ? asBuffer(bodyContent, container.options)
        : asBufferOrString(bodyContent);
};

const setupHeaders = (container, content) => {
    if (!content) return;
    const reqOpt = container.proxy.reqBuilder;
    reqOpt.headers['content-length'] = getContentLength(content);
    if (!container.options.reqBodyEncoding) return;
    reqOpt.headers['accept-charset'] = container.options.reqBodyEncoding;
};

export default (container) =>
    new Promise((resolve) => {
        const content = getBodyContent(container);
        setupHeaders(container, content);
        // eslint-disable-next-line no-param-reassign
        container.proxy.bodyContent = content;
        resolve(container);
    });
