/* eslint no-param-reassign: ["error", { "props": false }] */

import url from 'url';

const defaultProxyReqPathResolver = (ctx) => url.parse(ctx.url).path;

export default (container) => {
    const resolverFn =
        container.options.proxyReqPathResolver || defaultProxyReqPathResolver;

    return Promise.resolve(resolverFn(container.user.ctx)).then(
        (resolvedPath) => {
            container.proxy.reqBuilder.path = resolvedPath;
            return Promise.resolve(container);
        }
    );
};
