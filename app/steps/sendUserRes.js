/* eslint no-param-reassign: ["error", { "props": false }] */

const isHeaderSendOrTimeout = (ctx) => ctx.headerSent || ctx.status === 504;

export default (Container) => {
    if (!isHeaderSendOrTimeout(Container.user.ctx)) {
        Container.user.ctx.body = Container.proxy.resData;
    }
    return Promise.resolve(Container);
};
