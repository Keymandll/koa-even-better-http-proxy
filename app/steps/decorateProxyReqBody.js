/* eslint no-param-reassign: ["error", { "props": false }] */

const defaultDecorator = (proxyReqOptBody /* , userReq */) => proxyReqOptBody;

export default (container) => {
    const resolverFn =
        container.options.proxyReqBodyDecorator || defaultDecorator;

    return Promise.resolve(
        resolverFn(container.proxy.bodyContent, container.user.ctx)
    ).then((bodyContent) => {
        container.proxy.bodyContent = bodyContent;
        return Promise.resolve(container);
    });
};
