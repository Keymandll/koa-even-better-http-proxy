/* eslint no-param-reassign: ["error", { "props": false }] */

const defaultDecorator = (proxyReqOptBuilder /* , userReq */) =>
    proxyReqOptBuilder;
const getResolverFn = (options) =>
    options.proxyReqOptDecorator || defaultDecorator;

export default (container) => {
    const resolverFn = getResolverFn(container.options);
    return Promise.resolve(
        resolverFn(container.proxy.reqBuilder, container.user.ctx)
    ).then((processedReqOpts) => {
        delete processedReqOpts.params;
        container.proxy.reqBuilder = processedReqOpts;
        return Promise.resolve(container);
    });
};
