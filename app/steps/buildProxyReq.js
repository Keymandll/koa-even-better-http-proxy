/* eslint no-param-reassign: ["error", { "props": false }] */

import { bodyContent, createRequestOptions } from '../../lib/requestOptions';

export default (Container) => {
    const { ctx } = Container.user;
    const { options } = Container;
    const { host } = Container.proxy;

    const parseBody = !options.parseReqBody
        ? Promise.resolve(null)
        : bodyContent(ctx, options);
    const createReqOptions = createRequestOptions(ctx, options, host);

    return Promise.all([parseBody, createReqOptions]).then((responseArray) => {
        const [body, reqBuilder] = responseArray;
        Container.proxy.bodyContent = body;
        Container.proxy.reqBuilder = reqBuilder;
        return Container;
    });
};
