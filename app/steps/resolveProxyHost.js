/* eslint no-param-reassign: ["error", { "props": false }] */

import { parseHost } from '../../lib/requestOptions';

export default (container) => {
    const parsedHost = parseHost(container);
    container.proxy.reqBuilder.host = parsedHost.host;
    container.proxy.reqBuilder.port = container.options.port || parsedHost.port;
    container.proxy.requestModule = parsedHost.module;
    return Promise.resolve(container);
};
