import Koa from 'koa';
import { agent } from 'supertest';
import proxy from '..';
import proxyTarget from './support/proxyTarget';

describe('honors timeout option', () => {
    let other;
    let http;
    beforeEach(() => {
        http = new Koa();
        other = proxyTarget(8080, 1000, [
            {
                method: 'get',
                path: '/',
                fn(req, res) {
                    res.sendStatus(200);
                },
            },
        ]);
    });

    afterEach(() => {
        other.close();
    });

    function assertSuccess(server, done) {
        agent(http.callback())
            .get('/')
            .expect(200)
            .end((err) => {
                if (err) {
                    return done(err);
                }

                done();
            });
    }

    function assertConnectionTimeout(server, done) {
        agent(http.callback())
            .get('/')
            .expect(504)
            .expect(
                'X-Timout-Reason',
                'The proxy timed out your request after 100ms.'
            )
            .end((err) => {
                if (err) {
                    return done(err);
                }

                done();
            });
    }

    describe('when timeout option is set lower than server response time', () => {
        it('should fail with CONNECTION TIMEOUT', (done) => {
            http.use(
                proxy('http://localhost:8080', {
                    timeout: 100,
                })
            );

            assertConnectionTimeout(http, done);
        });
    });

    describe('when timeout option is set higher than server response time', () => {
        it('should succeed', (done) => {
            http.use(
                proxy('http://localhost:8080', {
                    timeout: 1200,
                })
            );

            assertSuccess(http, done);
        });
    });
});
