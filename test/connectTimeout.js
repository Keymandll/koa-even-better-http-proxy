import Koa from 'koa';
import { agent } from 'supertest';

import proxy from '..';
import proxyTarget from './support/proxyTarget';

describe('honors connectTimeout option', () => {
    let other;
    let http;
    beforeEach(() => {
        http = new Koa();
        other = proxyTarget(8080, 1000, [
            {
                method: 'get',
                path: '/',
                fn(_, res) {
                    res.sendStatus(200);
                },
            },
        ]);
    });

    afterEach(() => {
        other.close();
    });

    function assertSuccess(server, done) {
        agent(server.callback())
            .get('/')
            .expect(200)
            .end((err) => {
                if (err) {
                    return done(err);
                }

                done();
            });
    }

    function assertConnectionTimeout(server, time, done) {
        agent(server.callback())
            .get('/')
            .expect(504)
            .expect(
                'X-Timout-Reason',
                `The proxy timed out your request after ${time}ms.`
            )
            .end((err) => {
                if (err) {
                    return done(err);
                }

                done();
            });
    }

    describe('when connectTimeout option is set higher than server connect time', () => {
        it('should succeed', (done) => {
            http.use(
                proxy('http://localhost:8080', {
                    connectTimeout: 50,
                })
            );

            assertSuccess(http, done);
        });
    });

    describe('when timeout option is also used', () => {
        it('should fail with CONNECTION TIMEOUT when timeout is set lower than server response time', (done) => {
            http.use(
                proxy('http://localhost:8080', {
                    connectTimeout: 100,
                    timeout: 300,
                })
            );

            assertConnectionTimeout(http, 300, done);
        });

        it('should succeed when timeout is higher than server response time', (done) => {
            http.use(
                proxy('http://localhost:8080', {
                    connectTimeout: 100,
                    timeout: 1200,
                })
            );

            assertSuccess(http, done);
        });
    });
});
