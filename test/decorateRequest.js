import assert from 'assert';

import Koa from 'koa';
import { agent } from 'supertest';
import proxy from '..';

describe('proxyReqOptDecorator', function () {
    this.timeout(10000);

    describe('Supports Promise and non-Promise forms', () => {
        describe('when proxyReqOptDecorator is a simple function (non Promise)', () => {
            it('should mutate the proxied request', (done) => {
                const app = new Koa();
                app.use(
                    proxy('httpbin.org', {
                        proxyReqOptDecorator(reqOpt) {
                            reqOpt.headers['user-agent'] = 'test user agent';
                            return reqOpt;
                        },
                    })
                );

                agent(app.callback())
                    .get('/user-agent')
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }

                        assert.equal(res.body['user-agent'], 'test user agent');
                        done();
                    });
            });
        });

        describe('when proxyReqOptDecorator is a Promise', () => {
            it('should mutate the proxied request', (done) => {
                const app = new Koa();
                app.use(
                    proxy('httpbin.org', {
                        proxyReqOptDecorator(reqOpt) {
                            return new Promise((resolve) => {
                                reqOpt.headers['user-agent'] =
                                    'test user agent';
                                resolve(reqOpt);
                            });
                        },
                    })
                );

                agent(app.callback())
                    .get('/user-agent')
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }

                        assert.equal(res.body['user-agent'], 'test user agent');
                        done();
                    });
            });
        });
    });

    describe("proxyReqOptDecorator has access to the source request's data", () => {
        it('should have access to ip', (done) => {
            const app = new Koa();
            app.use(
                proxy('httpbin.org', {
                    proxyReqOptDecorator(reqOpts, ctx) {
                        assert(ctx.ip);
                        return reqOpts;
                    },
                })
            );

            agent(app.callback())
                .get('/')
                .end((err) => {
                    if (err) {
                        return done(err);
                    }

                    done();
                });
        });
    });
});
