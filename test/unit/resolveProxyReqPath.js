import { expect } from 'chai';
import assert from 'assert';

import ScopeContainer from '../../lib/scopeContainer';
import resolveProxyReqPath from '../../app/steps/resolveProxyReqPath';

describe('resolveProxyReqPath', () => {
    let container;

    beforeEach(() => {
        // TODO: mock `ctx`
        const context = {};
        container = ScopeContainer(context);
    });

    const tests = [
        {
            resolverType: 'undefined',
            resolverFn: undefined,
            data: [
                { url: 'http://localhost:12345', parsed: '/' },
                { url: 'http://g.com/123?45=67', parsed: '/123?45=67' },
            ],
        },
        {
            resolverType: 'a syncronous function',
            resolverFn() {
                return 'the craziest thing';
            },

            data: [
                { url: 'http://localhost:12345', parsed: 'the craziest thing' },
                { url: 'http://g.com/123?45=67', parsed: 'the craziest thing' },
            ],
        },
        {
            resolverType: 'a Promise',
            resolverFn() {
                return new Promise((resolve) => {
                    resolve('the craziest think');
                });
            },

            data: [
                { url: 'http://localhost:12345', parsed: 'the craziest think' },
                { url: 'http://g.com/123?45=67', parsed: 'the craziest think' },
            ],
        },
    ];

    describe('when proxyReqPathResolver', () => {
        tests.forEach((test) => {
            describe(`is ${test.resolverType}`, () => {
                describe('it returns a promise which resolves a container with expected url', () => {
                    test.data.forEach((data) => {
                        it(data.url, (done) => {
                            container.user.ctx = { url: data.url };
                            container.options.proxyReqPathResolver =
                                test.resolverFn;
                            const r = resolveProxyReqPath(container);

                            assert(
                                r instanceof Promise,
                                'Expect resolver to return a thennable'
                            );

                            r.then((container) => {
                                let response;
                                try {
                                    response = container.proxy.reqBuilder.path;
                                    if (!response) {
                                        throw new Error(
                                            'reqBuilder.url is undefined'
                                        );
                                    }
                                } catch (e) {
                                    done(e);
                                }

                                expect(response).to.equal(data.parsed);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });
});
