import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';

export default function proxyTarget(port, timeout, handlers) {
    const target = express();

    timeout = timeout || 100;

    // parse application/x-www-form-urlencoded
    target.use(bodyParser.urlencoded({ extended: false }));

    // parse application/json
    target.use(bodyParser.json());
    target.use(cookieParser());

    target.use((req, res, next) => {
        setTimeout(() => {
            next();
        }, timeout);
    });

    if (handlers) {
        handlers.forEach((handler) => {
            target[handler.method](handler.path, handler.fn);
        });
    }

    target.post('/post', (req, res) => {
        req.pipe(res);
    });

    target.use((err, req, res, next) => {
        res.send(err);
        next();
    });

    target.use((req, res) => {
        res.sendStatus(404);
    });

    return target.listen(port);
}
