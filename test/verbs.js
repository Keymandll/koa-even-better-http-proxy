import assert from 'assert';

import Koa from 'koa';
import { agent } from 'supertest';
import proxy from '..';

describe('http verbs', function () {
    this.timeout(10000);

    let app;

    beforeEach(() => {
        app = new Koa();
        app.use(proxy('httpbin.org'));
    });

    it('test proxy get', (done) => {
        agent(app.callback())
            .get('/get')
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                assert.equal(res.body.url, 'http://httpbin.org/get');
                done(err);
            });
    });

    it('test proxy post', (done) => {
        agent(app.callback())
            .post('/post')
            .send({
                mypost: 'hello',
            })
            .end((err, res) => {
                assert.equal(res.body.data, '{"mypost":"hello"}');
                done(err);
            });
    });

    it('test proxy put', (done) => {
        agent(app.callback())
            .put('/put')
            .send({
                mypost: 'hello',
            })
            .end((err, res) => {
                assert.equal(res.body.data, '{"mypost":"hello"}');
                done(err);
            });
    });

    it('test proxy patch', (done) => {
        agent(app.callback())
            .patch('/patch')
            .send({
                mypost: 'hello',
            })
            .end((err, res) => {
                assert.equal(res.body.data, '{"mypost":"hello"}');
                done(err);
            });
    });

    it('test proxy delete', (done) => {
        agent(app.callback())
            .del('/delete')
            .send({
                mypost: 'hello',
            })
            .end((err, res) => {
                assert.equal(res.body.data, '{"mypost":"hello"}');
                done(err);
            });
    });
});
