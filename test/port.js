import assert from 'assert';

import Koa from 'koa';
import { agent } from 'supertest';
import proxy from '..';

function proxyTarget(port) {
    const other = new Koa();
    other.use((ctx) => {
        ctx.status = 200;
        ctx.body = 'Success';
    });

    return other.listen(port);
}

describe('proxies to requested port', () => {
    let other;
    let http;
    beforeEach(() => {
        http = new Koa();
        other = proxyTarget(8080);
    });

    afterEach(() => {
        other.close();
    });

    function assertSuccess(server, done) {
        agent(http.callback())
            .get('/')
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                assert(res.text === 'Success');
                done();
            });
    }

    describe('when host is a String', () => {
        it('when passed as an option', (done) => {
            http.use(
                proxy('http://localhost', {
                    port: 8080,
                })
            );
            assertSuccess(http, done);
        });

        it('when passed on the host string', (done) => {
            http.use(proxy('http://localhost:8080'));
            assertSuccess(http, done);
        });
    });

    describe('when host is a function', () => {
        it('and port is on the String returned', (done) => {
            http.use(proxy(() => 'http://localhost:8080'));
            assertSuccess(http, done);
        });

        it('and port passed as an option', (done) => {
            http.use(
                proxy(
                    () => 'http://localhost',

                    { port: 8080 }
                )
            );
            assertSuccess(http, done);
        });
    });
});
