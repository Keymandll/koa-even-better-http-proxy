import assert from 'assert';

import Koa from 'koa';
import { agent } from 'supertest';
import proxy from '..';

describe('proxies https', function () {
    this.timeout(10000);

    let app;

    beforeEach(() => {
        app = new Koa();
    });

    function assertSecureRequest(app, done) {
        agent(app.callback())
            .get('/get?show_env=1')
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                assert(
                    res.body.headers['X-Forwarded-Port'] === '443',
                    'Expects forwarded 443 Port'
                );
                assert(
                    res.body.headers['X-Forwarded-Proto'] === 'https',
                    'Expects forwarded protocol to be https'
                );
                done();
            });
    }

    describe('when host is a String', () => {
        describe('and includes "https" as protocol', () => {
            it('proxys via https', (done) => {
                app.use(proxy('https://httpbin.org'));
                assertSecureRequest(app, done);
            });
        });

        describe('option https is set to "true"', () => {
            it('proxys via https', (done) => {
                app.use(proxy('http://httpbin.org', { https: true }));
                assertSecureRequest(app, done);
            });
        });
    });

    describe('when host is a Function', () => {
        describe('returned value includes "https" as protocol', () => {
            it('proxys via https', (done) => {
                app.use(proxy(() => 'https://httpbin.org'));

                assertSecureRequest(app, done);
            });
        });

        describe('option https is set to "true"', () => {
            it('proxys via https', (done) => {
                app.use(proxy(() => 'http://httpbin.org', { https: true }));

                assertSecureRequest(app, done);
            });
        });
    });
});
