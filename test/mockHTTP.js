import express from 'express';

const app = express();

app.use('/status/:status', (req, res) => {
    res.status(Number(req.params.status));
    res.send();
});

export default app;
