import assert from 'assert';

import Koa from 'koa';
import { agent } from 'supertest';
import proxy from '..';

describe('proxies headers', function () {
    this.timeout(10000);

    let http;

    beforeEach(() => {
        http = new Koa();
        http.use(
            proxy('http://httpbin.org', {
                headers: {
                    'X-Current-president': 'taft',
                },
            })
        );
    });

    it('does not include connection header by default', (done) => {
        const app = new Koa();
        app.use(
            proxy('httpbin.org', {
                proxyReqOptDecorator(reqOpts, ctx) {
                    try {
                        assert(!reqOpts.headers.connection);
                    } catch (err) {
                        done(err);
                    }

                    return ctx;
                },
            })
        );

        agent(app.callback())
            .get('/user-agent')
            .end((err) => {
                if (err) {
                    return done(err);
                }

                done();
            });
    });

    it('passed as options', (done) => {
        agent(http.callback())
            .get('/headers')
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                assert(res.body.headers['X-Current-President'] === 'taft');
                done();
            });
    });

    it('passed as on request', (done) => {
        agent(http.callback())
            .get('/headers')
            .set('X-Powerererer', 'XTYORG')
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                assert(res.body.headers['X-Powerererer']);
                done();
            });
    });
});
