import assert from 'assert';
import * as http from 'http';
import Koa from 'koa';

import { agent } from 'supertest';
import proxy from '../index';

describe('agent', function () {
    this.timeout(10000);

    it('agent', (done) => {
        const httpAgent = new http.Agent();
        const app = new Koa();
        app.use(
            proxy('httpbin.org', {
                agent: httpAgent,
                proxyReqOptDecorator(reqOpts, ctx) {
                    assert(reqOpts.agent, httpAgent);
                    return ctx;
                },
            })
        );

        agent(app.callback())
            .get('/user-agent')
            .end((err) => {
                if (err) {
                    return done(err);
                }

                done();
            });
    });
});
