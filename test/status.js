import Koa from 'koa';
import { agent } from 'supertest';

import proxy from '..';
import mockEndpoint from './mockHTTP.js';

describe('proxies status code', () => {
    const proxyServer = new Koa();
    const port = 21239;
    const proxiedEndpoint = `http://localhost:${port}`;
    let server;

    proxyServer.use(proxy(proxiedEndpoint));

    beforeEach(() => {
        server = mockEndpoint.listen(21239);
    });

    afterEach(() => {
        server.close();
    });

    [304, 404, 200, 401, 500].forEach((status) => {
        it(`on ${status}`, (done) => {
            agent(proxyServer.callback())
                .get(`/status/${status}`)
                .expect(status, done);
        });
    });
});
