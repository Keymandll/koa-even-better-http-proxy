import assert from 'assert';

import Koa from 'koa';
import { agent } from 'supertest';
import proxy from '..';

const execute = (app, done) => {
    agent(app.callback())
    .get('/')
    .end((err) => {
        if (err) {
            return done(err);
        }

        assert(true);
        done();
    });
}

describe('url parsing', function () {
    this.timeout(10000);

    it('can parse a url with a port', (done) => {
        const app = new Koa();
        app.use(proxy('http://httpbin.org:80'));
        execute(app, done);
    });

    it('does not throw `Uncaught RangeError` if you have both a port and a trailing slash', (done) => {
        const app = new Koa();
        app.use(proxy('http://httpbin.org:80/'));
        execute(app, done);
    });
});
