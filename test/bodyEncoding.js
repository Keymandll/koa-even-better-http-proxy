import assert from 'assert';
import { agent } from 'supertest';
import Koa from 'koa';
import { writeFileSync, unlinkSync } from 'fs';
import { tmpdir } from 'os';

import proxy from '..';
import startProxyTarget from './support/proxyTarget';

startProxyTarget(8109, 1000);

const pngHex =
    '89504e470d0a1a0a0' +
    '000000d4948445200' +
    '00000100000001080' +
    '60000001f15c48900' +
    '00000a49444154789' +
    'c6300010000050001' +
    '0d0a2db4000000004' +
    '9454e44ae426082';

const getLargeFile = (megabytes) => {
    const pngData = Buffer.from(pngHex, 'hex');
    const repeats = megabytes * 1024 * 1024 - pngData.length;
    return Buffer.from(pngData + 'A'.repeat(repeats));
};

const getFileName = () =>
    `${tmpdir()}/koa-http-proxy-test-${new Date().getTime()}-file.png`;

describe('body encoding', function () {
    this.timeout(10000);

    it('allow raw data', (done) => {
        const filename = getFileName();
        const fileData = getLargeFile(1);
        const app = new Koa();

        const proxyOptions = {
            reqBodyEncoding: null,
            proxyReqBodyDecorator(bodyContent) {
                // When processing requests with multipart content type body parsing is disabled!
                assert(bodyContent === null);
                return bodyContent;
            },
        };

        app.use(proxy('localhost:8109', proxyOptions));
        writeFileSync(filename, fileData);

        agent(app.callback())
            .post('/post')
            .attach('image', filename)
            .end((err) => {
                unlinkSync(filename);
                // This test is both broken and I think unnecessary.
                // Its broken because http.bin no longer supports /post, but this test assertion is based on the old
                // httpbin behavior.
                // The assertion in the proxyReqOptDecorator above verifies the test title.
                // var response = Buffer.from(res.body.attachment.data).toString('base64');
                // assert(response.indexOf(fileData.toString('base64')) >= 0, 'response should include original raw data');
                done(err);
            });
    });

    it('should not fail on large limit', (done) => {
        const filename = getFileName();
        const app = new Koa();
        const fileData = getLargeFile(5);

        const proxyOptions = {
            // This case `parseReqBody` should not be set to false,
            limit: '20gb',
        };

        app.use(proxy('localhost:8109', proxyOptions));
        writeFileSync(filename, fileData);

        agent(app.callback())
            .post('/post')
            .attach('image', filename)
            .expect(200)
            .end((err) => {
                unlinkSync(filename);
                assert(err === null);
                if (err) {
                    return done(err);
                }

                done();
            });
    });

    describe('when user sets parseReqBody', () => {
        it('should not parse body', (done) => {
            const filename = getFileName();
            const fileData = getLargeFile(7);
            const app = new Koa();

            const proxyOptions = {
                parseReqBody: false,
                proxyReqBodyDecorator(bodyContent) {
                    assert(!bodyContent, 'body content should not be parsed.');
                    return bodyContent;
                },
            };

            app.use(proxy('localhost:8109', proxyOptions));
            writeFileSync(filename, fileData);

            agent(app.callback())
                .post('/post')
                .attach('image', filename)
                .end((err) => {
                    unlinkSync(filename);
                    // This test is both broken and I think unnecessary.
                    // Its broken because http.bin no longer supports /post, but this test assertion is based on the old
                    // httpbin behavior.
                    // The assertion in the proxyReqOptDecorator above verifies the test title.
                    // var response = new Buffer(res.body.attachment.data).toString('base64');
                    // assert(response.indexOf(fileData.toString('base64')) >= 0, 'response should include original raw data');
                    done(err);
                });
        });
    });

    describe('when user sets reqBodyEncoding', () => {
        it('should set the accepts-charset header', (done) => {
            const app = new Koa();

            const proxyOptions = {
                reqBodyEncoding: 'utf-16',
            };

            app.use(proxy('httpbin.org', proxyOptions));
            agent(app.callback())
                .get('/headers')
                .end((err, res) => {
                    if (err) {
                        throw err;
                    }

                    assert.equal(res.body.headers['Accept-Charset'], 'utf-16');
                    done(err);
                });
        });
    });
});
