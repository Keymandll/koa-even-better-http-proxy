import assert from 'assert';

import Koa from 'koa';
import { agent } from 'supertest';

import proxy from '..';
import proxyTarget from './support/proxyTarget';

const proxyRouteFn = [
    {
        method: 'get',
        path: '/cookieTest',
        fn(req, res) {
            Object.keys(req.cookies).forEach((key) => {
                res.cookie(key, req.cookies[key]);
            });

            res.sendStatus(200);
        },
    },
];

describe('proxies cookie', function () {
    this.timeout(10000);

    let app;
    let proxyServer;

    beforeEach(() => {
        proxyServer = proxyTarget(12346, 100, proxyRouteFn);
        app = new Koa();
        app.use(proxy('localhost:12346'));
    });

    afterEach(() => {
        proxyServer.close();
    });

    it('set cookie', (done) => {
        agent(app.callback())
            .get('/cookieTest')
            .set('Cookie', 'myApp-token=12345667')
            .end((err, res) => {
                const cookiesMatch = res.headers['set-cookie'].filter((item) =>
                    item.match(/myApp-token=12345667/)
                );

                assert(cookiesMatch);
                done(err);
            });
    });
});
