import assert from 'assert';
import * as http from 'http';
import Koa from 'koa';
import { agent } from 'supertest';

import proxy from '../..';
import proxyTarget from '../support/proxyTarget';

describe('resolveProxyReqPath', function () {
    let server;

    this.timeout(10000);

    before(() => {
        const handlers = [
            {
                method: 'get',
                path: '/working',
                fn(req, res) {
                    res.sendStatus(200);
                },
            },
        ];

        server = proxyTarget(12345, 100, handlers);
    });

    after(() => {
        server.close();
    });

    describe('when author uses option proxyReqPathResolver', () => {
        it('the proxy request path is the result of the function', (done) => {
            const app = new Koa();
            const opts = {};
            opts.proxyReqPathResolver = function () {
                return '/working';
            };

            app.use(proxy('localhost:12345', opts));

            agent(app.callback()).get('/failing').expect(200).end(done);
        });

        it('the proxyReqPathResolver method has access to request object', (done) => {
            const app = new Koa();
            const opts = {};
            opts.proxyReqPathResolver = function (ctx) {
                assert.ok(ctx.req instanceof http.IncomingMessage);
                return '/working';
            };

            app.use(proxy('localhost:12345', opts));

            agent(app.callback()).get('/foobar').expect(200).end(done);
        });
    });
});
