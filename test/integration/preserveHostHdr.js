import assert from 'assert';

import Koa from 'koa';
import { agent } from 'supertest';

import proxy from '../..';
import proxyTarget from '../support/proxyTarget';

const checkHostHeader = (app, opts, value, expected, done) => {
    app.use(proxy('localhost:12346', opts));
    agent(app.callback())
        .set('Host', value)
        .get('/test')
        .expect(200)
        .end((_err, res) => {
            assert(res.body.host === expected);
            done();
        });
}

describe('preserveHostHdr', function () {
    let server;

    this.timeout(10000);

    before(() => {
        const handlers = [
            {
                method: 'get',
                path: '/test',
                fn(req, res) {
                    res.send({ host: req.headers.host });
                },
            },
        ];

        server = proxyTarget(12346, 100, handlers);
    });

    after(() => {
        server.close();
    });

    describe('when author does not use option preserveHostHdr', () => {
        it('the host header should not be preserved', (done) => {
            const app = new Koa();
            const opts = {};
            checkHostHeader(app, opts, 'libtest', 'localhost:12346', done);
        });
    });

    describe('when author uses option preserveHostHdr', () => {
        it('the host header must be preserved', (done) => {
            const app = new Koa();
            const opts = {};
            opts.preserveHostHdr = true;
            checkHostHeader(app, opts, 'libtest', 'libtest', done);
        });
    });
});
