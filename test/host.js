import Koa from 'koa';
import { agent } from 'supertest';
import proxy from '..';

describe('host can be a dynamic function', function () {
    this.timeout(10000);

    const app = new Koa();
    const firstProxyApp = new Koa();
    const secondProxyApp = new Koa();
    const firstPort = 10001;
    const secondPort = 10002;

    app.use(proxy((ctx) => `localhost:${ctx.url.replace('/proxy/', '')}`));

    firstProxyApp.use((ctx) => {
        ctx.status = 204;
    });

    firstProxyApp.listen(firstPort);

    secondProxyApp.use((ctx) => {
        ctx.status = 200;
    });

    secondProxyApp.listen(secondPort);

    it('can proxy with session value', (done) => {
        agent(app.callback())
            .get(`/proxy/${firstPort}`)
            .expect(204)
            .end((err) => {
                if (err) {
                    return done(err);
                }

                agent(app.callback())
                    .get(`/proxy/${secondPort}`)
                    .expect(200, done);
            });
    });
});
