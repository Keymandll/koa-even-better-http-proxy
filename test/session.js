import assert from 'assert';

import Koa from 'koa';
import { agent } from 'supertest';
import proxy from '..';

describe('preserveReqSession', function () {
    this.timeout(10000);

    it('preserveReqSession', (done) => {
        const app = new Koa();
        app.use((ctx, next) => {
            ctx.session = 'hola';
            return Promise.resolve(null).then(next);
        });

        app.use(
            proxy('httpbin.org', {
                preserveReqSession: true,
                proxyReqOptDecorator(reqOpts, ctx) {
                    assert(reqOpts.session, 'hola');
                    return ctx;
                },
            })
        );

        agent(app.callback())
            .get('/user-agent')
            .end((err) => {
                if (err) {
                    return done(err);
                }

                done();
            });
    });
});
