import assert from 'assert';

import ScopeContainer from './lib/scopeContainer';

import prepareProxyReq from './app/steps/prepareProxyReq';
import decorateUserRes from './app/steps/decorateUserRes';
import sendProxyRequest from './app/steps/sendProxyRequest';
import buildProxyReq from './app/steps/buildProxyReq';
import copyProxyResHeadersToUserRes from './app/steps/copyProxyResHeadersToUserRes';
import decorateProxyReqBody from './app/steps/decorateProxyReqBody';
import decorateProxyReqOpts from './app/steps/decorateProxyReqOpts';
import resolveProxyHost from './app/steps/resolveProxyHost';
import resolveProxyReqPath from './app/steps/resolveProxyReqPath';
import sendUserRes from './app/steps/sendUserRes';

export default function proxy(host, userOptions) {
    assert(host, 'Host should not be empty');
    return (ctx, next) => {
        const container = ScopeContainer(ctx, host, userOptions);

        // Skip proxy if filter is falsey.  Loose equality so filters can return
        // false, null, undefined, etc.
        if (!container.options.filter(ctx)) {
            return Promise.resolve(null).then(next);
        }

        return buildProxyReq(container)
            .then(resolveProxyHost)
            .then(decorateProxyReqOpts)
            .then(resolveProxyReqPath)
            .then(decorateProxyReqBody)
            .then(prepareProxyReq)
            .then(sendProxyRequest)
            .then(copyProxyResHeadersToUserRes)
            .then(decorateUserRes)
            .then(sendUserRes)
            .then(next);
    };
}
