import _ from 'lodash';
import resolveOptions from './resolveOptions';

// The Container object is passed down the chain of Promises, with each one
// of them mutating and returning Container.  The goal is that (eventually)
// author using this library could hook into/override any of these workflow
// steps with a Promise or simple function.
// Container for scoped arguments in a promise chain. Each promise recieves
// this as its argument, and returns it.
//
// Do not expose the details of this to hooks/user functions.

export default (ctx, host, userOptions) => {
    // Ensure multipart/form-data messages handled properly by forcing
    // `parseReqBody` to be `false`.
    const options = _.cloneDeep(userOptions);
    const contentType = _.get(ctx, ['request', 'header', 'content-type'], '');
    if (contentType.toLowerCase().startsWith('multipart/form-data')) {
        options.parseReqBody = false;
    }

    return {
        user: {
            ctx,
        },
        proxy: {
            req: {},
            res: {},
            resData: undefined, // from proxy res
            bodyContent: undefined, // for proxy req
            reqBuilder: {}, // reqOpt, intended as first arg to http(s)?.request
        },
        options: resolveOptions(options),
        params: {
            host,
            options,
        },
    };
};
