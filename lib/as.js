const getValue = (body) =>
    typeof body === 'object' ? JSON.stringify(body) : body;
const isObjectOrString = (value) => ['object', 'string'].includes(typeof value);

export const asBuffer = (body, options) => {
    if (!isObjectOrString(body)) {
        return null;
    }

    return Buffer.isBuffer(body)
        ? body
        : Buffer.from(getValue(body), options.reqBodyEncoding);
};

export const asBufferOrString = (body) => {
    if (!isObjectOrString(body)) {
        return null;
    }

    return Buffer.isBuffer(body) ? body : getValue(body);
};
