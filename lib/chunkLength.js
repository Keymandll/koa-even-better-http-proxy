export default (chunks) => chunks.reduce((len, buf) => len + buf.length, 0);
