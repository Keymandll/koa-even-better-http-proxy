import _ from 'lodash';
import isUnset from './isUnset';

// No-op version of filter.  Allows everything!
const defaultFilter = () => true;

const initOptions = (options) => (options ? _.cloneDeep(options) : {});
const getFilter = (options) => options.filter || defaultFilter;
const shouldParseBody = (options) =>
    isUnset(options.parseReqBody) ? true : options.parseReqBody;

/* For reqBodyEncoding, these is a meaningful difference between null and
 * undefined.  null should be passed forward as the value of reqBodyEncoding,
 * and undefined should result in utf-8.
 */
const resolveBodyEncoding = (reqBodyEncoding) =>
    reqBodyEncoding !== undefined ? reqBodyEncoding : 'utf-8';

export default (options) => {
    // resolve user argument to program usable options
    const opts = initOptions(options);

    return {
        agent: opts.agent,
        proxyReqPathResolver: opts.proxyReqPathResolver,
        proxyReqOptDecorator: opts.proxyReqOptDecorator,
        proxyReqBodyDecorator: opts.proxyReqBodyDecorator,
        userResDecorator: opts.userResDecorator,
        userResHeadersDecorator: opts.userResHeadersDecorator,
        filter: getFilter(opts),
        // For backwards compatability, we default to legacy behavior for newly added settings.
        parseReqBody: shouldParseBody(opts),
        reqBodyEncoding: resolveBodyEncoding(opts.reqBodyEncoding),
        headers: opts.headers,
        strippedHeaders: opts.strippedHeaders,
        preserveReqSession: opts.preserveReqSession,
        preserveHostHdr: opts.preserveHostHdr,
        https: opts.https,
        port: opts.port,
        reqAsBuffer: opts.reqAsBuffer,
        connectTimeout: opts.connectTimeout,
        timeout: opts.timeout,
        limit: opts.limit,
    };
};
