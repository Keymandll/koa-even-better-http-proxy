export default (val) =>
    typeof val === 'undefined' || val === '' || val === null;
